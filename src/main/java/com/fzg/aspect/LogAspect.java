package com.fzg.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * create by fzg
 * 2021/11/23 10:55
 * 日志处理切面
 */

@Aspect
@Component
public class LogAspect {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    // 拦截web下所有类的方法
    @Pointcut("execution(* com.fzg.web.*.*(..))")
    public void log(){

    }

    // 切面之前
    @Before("log()")
    public void doBefore(JoinPoint joinPoint){
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String url = request.getRequestURI();
        String ip = request.getRemoteAddr();
        String classMethod = joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();

        RequestLog requestLog = new RequestLog(url, ip, classMethod, args);

        logger.info("请求对象： {}",requestLog);
    }

    // after
    @After("log()")
    public void doAfter(){
    }

    // 方法返回
    @AfterReturning(returning = "result",pointcut = "log()")
    public void doAfterReturn(Object result){
        logger.info("Result: {}",result);
    }

    private class RequestLog{
        private String url;
        private String ip;
        private String classMetgod;
        private Object[] args;

        public RequestLog(String url, String ip, String classMetgod, Object[] args) {
            this.url = url;
            this.ip = ip;
            this.classMetgod = classMetgod;
            this.args = args;
        }

        @Override
        public String toString() {
            return "RequestLog{" +
                    "url='" + url + '\'' +
                    ", ip='" + ip + '\'' +
                    ", classMetgod='" + classMetgod + '\'' +
                    ", args=" + Arrays.toString(args) +
                    '}';
        }
    }

}
