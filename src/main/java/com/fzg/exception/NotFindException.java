package com.fzg.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * create by fzg
 * 2021/11/23 10:45
 */

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFindException extends RuntimeException{

    public NotFindException() {
    }

    public NotFindException(String message) {
        super(message);
    }

    public NotFindException(String message, Throwable cause) {
        super(message, cause);
    }
}
