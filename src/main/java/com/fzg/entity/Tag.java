package com.fzg.entity;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * create by fzg
 * 2021/11/23 17:22
 */

@Data
@Entity
@Table(name = "`t_tag`")
public class Tag {

    /**
     * 标签id
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * 标签名称
     */
    @NotBlank(message = "标签名称不能为空")
    private String name;

    @ManyToMany(mappedBy = "tags")
    private List<Blog> blogs = new ArrayList<>();

}
