package com.fzg.entity;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * create by fzg
 * 2021/11/23 17:20
 */

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "`t_type`")
public class Type {

    /**
     * 分类ID
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * 分类名称
     */
    @NotBlank(message = "分类名称不能为空")
    private String name;

    @OneToMany(mappedBy = "type")
    @ToString.Exclude
    private List<Blog> blogs = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Type type = (Type) o;
        return id != null && Objects.equals(id, type.id);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
