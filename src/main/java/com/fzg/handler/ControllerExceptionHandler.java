package com.fzg.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;


/**
 * create by fzg
 * 2021/11/23 9:51
 */

@ControllerAdvice
public class ControllerExceptionHandler {

    private final Logger logger = (Logger) LoggerFactory.getLogger(ControllerExceptionHandler.class);

    /**
     * 异常处理
     */
    @ExceptionHandler(Exception.class)
    public ModelAndView handleException(HttpServletRequest request,Exception e) throws Exception{
        logger.error("请求地址： {} , 异常： {}",request.getRequestURI(),e);

        if (AnnotationUtils.findAnnotation(e.getClass(),ResponseStatus.class) != null){
            throw e;
        }

        ModelAndView mv = new ModelAndView();
        mv.addObject("url",request.getRequestURI());
        mv.addObject("exception",e);
        mv.setViewName("error/error");

        return mv;
    }

}
