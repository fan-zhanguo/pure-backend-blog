package com.fzg.service;

import com.fzg.entity.Comment;

import java.util.List;

public interface CommentService {

    List<Comment> listCommentByBlogId(Long blogId);

    Comment  saveComment(Comment comment);

}
