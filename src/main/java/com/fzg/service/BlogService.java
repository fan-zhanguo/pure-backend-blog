package com.fzg.service;

import com.fzg.entity.Blog;
import com.fzg.model.BlogQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * create by fzg
 * 2021/11/25 15:03
 */


public interface BlogService {

    Blog getBlog(Long id);

    Page<Blog> listBlog(Pageable pageable, BlogQuery blog);

    Blog saveBlog(Blog blog);

    Blog updateBlog(Long id, Blog blog);

    void deleteBlog(Long id);

    Page<Blog> listBlog(Pageable pageable);

    List<Blog> listRecommendBlogTop(Integer size);

    Page<Blog> listBlog(Pageable pageable, String query);

    Blog getAndConvert(Long id);

    Page<Blog> listBlog(Long tagId, Pageable pageable);

    Map<String,List<Blog>> archiveBlog();

    Long countBlog();

}
