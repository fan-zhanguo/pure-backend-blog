package com.fzg.service;

import com.fzg.entity.User;

public interface UserService {

    User checkUser(String username, String password);

}
