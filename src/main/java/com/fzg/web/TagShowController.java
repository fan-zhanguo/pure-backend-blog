package com.fzg.web;

import com.fzg.entity.Tag;
import com.fzg.entity.Type;
import com.fzg.model.BlogQuery;
import com.fzg.service.BlogService;
import com.fzg.service.TagService;
import com.fzg.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * create by fzg
 * 2021/12/1 10:04
 */

@Controller
public class TagShowController {

    @Autowired
    private TagService tagService;

    @Autowired
    private BlogService blogService;

    @GetMapping("/tags/{id}")
    public String types(@PathVariable Long id,
                        @PageableDefault(size = 6,sort = {"updateTime"},direction = Sort.Direction.DESC)
                                Pageable pageable,
                        Model model){
        List<Tag> tags = tagService.listTagTop(10000);
        if (id == -1){
            id = tags.get(0).getId();
        }
        model.addAttribute("tags",tags);
        model.addAttribute("page",blogService.listBlog(id,pageable));
        model.addAttribute("activeTagId",id);
        return "tags";
    }
}
