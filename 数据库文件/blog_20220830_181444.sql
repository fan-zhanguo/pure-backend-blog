-- MySQL dump 10.13  Distrib 5.7.38, for Linux (x86_64)
--
-- Host: localhost    Database: blog
-- ------------------------------------------------------
-- Server version	5.7.38-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (83);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_blog`
--

DROP TABLE IF EXISTS `t_blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_blog` (
  `id` bigint(20) NOT NULL,
  `appreciation` bit(1) NOT NULL,
  `commentabled` bit(1) NOT NULL,
  `content` longtext COLLATE utf8_bin,
  `create_time` datetime(6) DEFAULT NULL,
  `first_picture` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `flag` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `published` bit(1) NOT NULL,
  `recommend` bit(1) NOT NULL,
  `share_statement` bit(1) NOT NULL,
  `title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK522hflrfk6jj4e0uup8x2abyt` (`type_id`) USING BTREE,
  KEY `FKn7j6fs0qph98c68oyomw1icwm` (`user_id`) USING BTREE,
  CONSTRAINT `FK522hflrfk6jj4e0uup8x2abyt` FOREIGN KEY (`type_id`) REFERENCES `t_type` (`id`),
  CONSTRAINT `FKn7j6fs0qph98c68oyomw1icwm` FOREIGN KEY (`user_id`) REFERENCES `t_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_blog`
--

LOCK TABLES `t_blog` WRITE;
/*!40000 ALTER TABLE `t_blog` DISABLE KEYS */;
INSERT INTO `t_blog` VALUES (18,_binary '\0',_binary '','## 微信公众号格式化工具\r\n\r\n> 使用微信公众号编辑器有一个十分头疼的问题——粘贴出来的代码，格式错乱，而且特别丑。这块编辑器能够解决这个问题。\r\n\r\n**在这里粘贴您的Markdown文档，点击“预览”按钮转换为HTML格式。** \r\n\r\n## 脚本之家公众号\r\n\r\n![微信公众号](http://icws.jb51.net/images/weixin_jb51.gif)\r\n\r\n## Markdown基础语法\r\n\r\n下面是Markdown的常用语法示例，可供参考\r\n\r\n### 代码示例\r\n\r\n```javascript\r\nvar OnlineMarkdown = {\r\n  init: function() {\r\n    var self = this;\r\n    self.load().then(function() {\r\n      self.start()\r\n    }).fail(function(){\r\n      self.start();\r\n    });\r\n  },\r\n  start: function() {\r\n    this.updateOutput();\r\n  },\r\n  load: function() {\r\n    return $.ajax({\r\n      type: \'GET\',\r\n      url: params.path || \'./demo.md\',\r\n      dateType: \'text\',\r\n      timeout: 2000\r\n    }).then(function(data) {\r\n      $(\'#input\').val(data);\r\n    });\r\n  },\r\n  updateOutput: function () {\r\n    var val = this.converter.makeHtml($(\'#input\').val());\r\n    $(\'#output .wrapper\').html(val);\r\n    PR.prettyPrint();\r\n  }\r\n};\r\n\r\nOnlineMarkdown.init();\r\n```\r\n---\r\n\r\n上面是 `JavaScript`，下面是 `php`：\r\n\r\n```php\r\necho \'hello,world\'\r\n```\r\n\r\n### 表格示例\r\n\r\n| 品类 | 个数 | 备注 |\r\n|-----|-----|------|\r\n| 苹果 | 1   | nice |\r\n| 橘子 | 2   | job |\r\n\r\n\r\n\r\n---\r\n\r\n以上是用的比较多的，还装了几十个使用频度比较低的插件，主要包括 Snippet 和文件高亮配置，可以在这里查看：<https://gist.github.com/barretlee/a5170eb6ca1805f66687063d2e3a4983>，你也可以通过 `Settings Sync` 将这个配置下载下来，id 就是后面半截：`a5170eb6ca1805f66687063d2e3a4983`。\r\n\r\n### 在命令行打开 VSC\r\n\r\n在安装好 VSC 后，直接配置 `.bash_profile` 或者 `.zshrc` 文件：\r\n\r\n```bash\r\nalias vsc=\'/Applications/Visual\\ Studio\\ Code.app/Contents/Resources/app/bin/code\';\r\nVSC_BIN=\'/Applications/Visual\\ Studio\\ Code.app/Contents/Resources/app/bin\';\r\nPATH=$VSC_BIN:$PATH;\r\nexport PATH;\r\n```\r\n\r\n然后让配置生效，在控制台执行：\r\n\r\n```bash\r\n# 如果没有安装 zsh，可能是 ~/.bash_profile\r\nsource ~/.zshrc \r\n```\r\n\r\n这个时候就可以在全局打开了：\r\n\r\n```bash\r\n# -a 的意思是不要新开窗口，在当前已经打开的 vsc 中打开文件\r\nvsc path/to/file.ext -a \r\n```\r\n\r\n有同学提到，VSC 的面板上搜索 `install` 就可以在命令行安装 `code` 这个命令了，不过我更喜欢使用 `vsc` 来打开文件，这也算是折腾吧 ；）\r\n',NULL,'https://img0.baidu.com/it/u=3480104072','转载',_binary '',_binary '',_binary '','脚本之家—markdown语法','2022-04-15 17:54:14.839000',19,3,1,'是一个Markdown语法，在脚本之家网站里。cdsa'),(65,_binary '',_binary '','### pretty girl\r\n\r\n[![pretty girl](http://obohe.com/i/2021/12/01/nonzej.jpeg \"pretty girl\")](http://obohe.com/i/2021/12/01/nonzej.jpeg \"pretty girl\")\r\n\r\nthis is my girlfriend！\r\nhaha.\r\n2021-12-01 14:35:14 星期三\r\n\r\n',NULL,'https://s3.bmp.ovh/imgs/2021/12/2ad7105dd814c821.jpg','原创',_binary '',_binary '',_binary '','照片','2021-12-01 14:56:52.980000',9,1,1,'pretty girl'),(69,_binary '',_binary '','## 神笔马良\r\n#### 神话故事导读：\r\n&emsp;&emsp;从前，有个叫马良的穷孩子，他天生聪敏，从小喜欢画画。可是由于家里 穷困潦倒，他连买一支笔的钱也没有 他到山上打柴时，就折一根树枝在山坡 上画；到河边割草时，就用草根蘸着河水在河边画；回到家里，就拿一块木炭 在院子里画。\r\n\r\n#### 正文：\r\n&emsp;&emsp;马良坚持不懈地画画，从没有间断过一天。但他常常想，如果自己能有一支画笔那该有多好呀 一个晚上，马良恍惚中感到窑洞里亮起了一阵五彩的光芒，这时出现了一个白胡子老人，老人送给他一支金光灿灿的神笔。马良高兴地惊醒过来，原来是个梦！可他看看自己的手，真是太不可思议了 自己手里确实有一枝笔。他马上用笔画了一只鸟，鸟竟活了过来，展开翅膀飞了起来，他又画了一条鱼，鱼也活了起来，活蹦乱跳。马良有了这支神笔，天天替村子里穷苦善良的人家画画，谁家缺什么，马良就给他们画什么。\r\n\r\n&emsp;&emsp;邻村的一个贪婪、为富不仁的大财主听说这件事以后，马上派人将马良抓了过去，逼他为自己画画。无论财主如何哄他、吓他，他就是不肯画。财主把他关到了马厩里，不给他饭吃。傍晚下起了鹅毛大雪。财主见马厩的门缝里透出红色的亮光，还闻到一股香喷喷的味道，就向门里看，马良在里面烧起了一个大火炉，边烤着火，边吃着热烘烘的饼子。这火炉和饼子都是马良用神笔画出来的。财主顿时怒火中烧，打算把马良杀死，夺下他的神笔。这时马良攀上一架梯子，翻墙走了。财主急忙攀上梯子去追，刚爬了两步，就摔了下来。原来，这梯子也是马良用神笔画的。财主还没爬起来，马良已骑着一匹用神笔画的骏马飞奔而去。\r\n\r\n&emsp;&emsp;财主骑着马，带着人，追了上来。眼看就要追着了，马良用神笔画了一张弓、一枝箭。马良搭弓射箭，一箭射中了财主的咽喉，财主顿时气绝身亡。皇帝知道后，派人把马良抓了去。皇帝威逼马良给他画株摇钱树，否则的话，就要将马良杀掉。马良挥起神笔，在一个无边的大海中央有一个小岛，岛上有一株又高又大的摇钱树。马良又画了一只巨大的木船，皇帝带上人上了木船。马良又画了几笔风，大木船顺风而行。马良继续不停地画风，海风卷起一层层的巨浪，船被巨浪打翻了，皇帝也沉到了海底。\r\n\r\n&emsp;&emsp;马良后来到底去了什么地方 ，人们不得而知 。有人说 ，他回到了自己的家乡，和那些种地的伙伴在一起。也有人说，他到处流浪，专门给穷苦的人们画画。\r\n',NULL,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F11372831312%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1640933412&t=44d03f3bfdd3e738f59eb512bf471e55','原创',_binary '',_binary '',_binary '','神笔马良','2021-12-01 14:59:39.680000',8,68,1,'从前，有个叫马良的穷孩子，他天生聪敏，从小喜欢画画。可是由于家里 穷困潦倒，他连买一支笔的钱也没有 他到山上打柴时，就折一根树枝在山坡 上画；到河边割草时，就用草根蘸着河水在河边画；回到家里，就拿一块木炭 在院子里画。'),(73,_binary '',_binary '','## 欧罗巴\r\n##### 神话故事导读:\r\n&emsp;&emsp;阿革诺耳国王的女儿，美丽的公主欧罗巴，在一个深夜里， 天神给了她一个奇异的梦。好像两块大陆一块是亚细亚；一 块是亚细亚对面的大陆变成了两个妇女模样。一个是当地人 打扮，一个是外乡人打扮。她俩为抢夺她而互相斗争着。当地的 妇女说她哺育了她；\r\n\r\n##### 正文\r\n&emsp;&emsp;阿革诺耳国王的女儿，美丽的公主欧罗巴，在一个深夜里，天神给了她一个奇异的梦。好像两块大陆———一块是亚细亚；一块是亚细亚对面的大陆———变成了两个妇女模样。一个是当地人打扮，一个是外乡人打扮。她俩为抢夺她而互相斗争着。当地的妇女说她哺育了她；外乡妇女则用强有力的手将她抱在怀里，并将她带走，说命运女神指定她将作为宙斯的情人。\r\n\r\n&emsp;&emsp;欧罗巴醒来，只觉面孔发烧，她不知道这梦是吉兆还是凶兆。\r\n\r\n&emsp;&emsp;清晨，阳光灿烂，欧罗巴暂时忘掉了她的梦，和女伴们一起来到海边开放着许多花朵的草地。女伴们兴高采烈地分散在草地四周，采撷着美丽的鲜花。欧罗巴也挎着一只金花篮，在草地上跑着，采摘她心爱的花朵。她采到了一枝特大的火焰一般的红玫瑰。\r\n\r\n&emsp;&emsp;众神之父宙斯被爱神阿佛洛狄忒的金箭射中，为年轻美丽的欧罗巴而神魂颠倒。他隐去神的真形，化成了一条牡牛。这样，既可骗取年轻姑娘的柔情，也可能躲避神后赫拉愤怒的妒火。\r\n\r\n&emsp;&emsp; 牡牛来到欧罗巴的面前。它长得是那样美丽，像人工雕琢的双角，金黄色的身体，前额中闪烁着一个新月形的银色标记，亮蓝的眼睛里闪耀着柔和的光，显得十分温驯。欧罗巴很喜爱这条牛，她温柔地抚摸着它的背，给它拭去嘴上的泡沫，然后吻了一下它的前额。这是牡牛快乐地鸣叫一声，在欧罗巴的脚边卧了下来，昂着头，望着她，向她展示它那宽大的脊背。\r\n\r\n&emsp;&emsp;欧罗巴喊着她的伙伴们：“快过来呀，姑娘们！让我们骑到  这只美丽牡牛的背上玩吧。我保证，我们都可以坐得下。看，这  牛是多么温顺，多么可爱呀！我相信它和人类一样有颗善良的心，只是不会说话罢了。”\r\n\r\n&emsp;&emsp;说着她灵巧地跨上了牛背，她的同伴们则踌躇害怕，不敢上前。\r\n\r\n&emsp;&emsp;牡牛得到了它的意中人，立即从地上跃起，起初是缓缓地走  着，当到了大海边的时候，立即跃身向大海奔去。欧罗巴吓得大   声喊叫，向她的伙伴们伸出手来，但她的同伴们已够不着她了。\r\n\r\n&emsp;&emsp;牡牛四蹄轻巧地踏在海面上，不沾一滴水，像骏马在柔软的    草原上奔驰。欧罗巴害怕得双手紧握着它的角，怯怯地说道： “神牛啊，你要把我带到什么地方去？大海是鱼类的运动场，不是牛行的航道。你用脚在上面走，你一定是一位神明，因为你的行事，只有神才能做到。”\r\n\r\n&emsp;&emsp;那牡牛答道：“不要害怕，姑娘。我是奥林匹斯山众神之王。出于对你的爱，我才变成了牛身，在海中这样奔波。不久，将有一块新的陆地收容你。我们的新房也将安在那里。”\r\n\r\n&emsp;&emsp;不久，欧罗巴故乡的陆地渐渐消失，太阳开始西沉，天黑下来。夜晚，只有星光和浪花与她作伴。\r\n\r\n&emsp;&emsp;第二天，牡牛还在海上游行。晚上，他们到达了一块远方的新陆地。牡牛爬上岸，让姑娘从它的背上滑下来。突然间，牡牛消失，出现了一位美如天神的青年男子。\r\n\r\n&emsp;&emsp;从此，欧罗巴成了宙斯的人间妻子，这块陆地也因欧罗巴公主而得名叫 “欧洲大陆。”','2021-12-01 16:26:38.133000','https://bkimg.cdn.bcebos.com/pic/b2de9c82d158ccbf5a88f1f71bd8bc3eb03541c2?x-bce-process=image/watermark,image_d2F0ZXIvYmFpa2U4MA==,g_7,xp_5,yp_5/format,f_auto','',_binary '',_binary '',_binary '','欧罗巴','2021-12-01 16:26:38.133000',9,68,1,'欧罗巴的故事'),(82,_binary '',_binary '','[![星空](https://t7.baidu.com/it/u=2638406194,523661981&fm=193&f=GIF \"星空\")](https://t7.baidu.com/it/u=2638406194,523661981&fm=193&f=GIF \"星空\")','2022-08-05 11:48:44.039000','https://t7.baidu.com/it/u=2638406194,523661981&fm=193&f=GIF','原创',_binary '',_binary '',_binary '','星空','2022-08-05 11:48:44.039000',4,1,1,'去青海寻找夜空中最亮的星，位于青海省海西蒙古族藏族自治州乌兰县茶卡镇附近的茶卡盐湖，素有中国“天空之镜”的美誉。白天可以看到澄清的天空之镜，夜晚抬头就能看见满天的星光，在空无一人的茶卡盐湖上空，星光明亮而灿烂。盐湖如镜，将银河与繁星倒映的清晰透亮。');
/*!40000 ALTER TABLE `t_blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_blog_tags`
--

DROP TABLE IF EXISTS `t_blog_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_blog_tags` (
  `blogs_id` bigint(20) NOT NULL,
  `tags_id` bigint(20) NOT NULL,
  KEY `FKlnh618jormtgyty7hlscr977v` (`tags_id`) USING BTREE,
  KEY `FK5p5mntpulvakdbqpdb6a2s2g4` (`blogs_id`) USING BTREE,
  CONSTRAINT `FK5p5mntpulvakdbqpdb6a2s2g4` FOREIGN KEY (`blogs_id`) REFERENCES `t_blog` (`id`),
  CONSTRAINT `FKlnh618jormtgyty7hlscr977v` FOREIGN KEY (`tags_id`) REFERENCES `t_tag` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_blog_tags`
--

LOCK TABLES `t_blog_tags` WRITE;
/*!40000 ALTER TABLE `t_blog_tags` DISABLE KEYS */;
INSERT INTO `t_blog_tags` VALUES (65,14),(69,9),(69,70),(69,71),(73,15),(73,71),(18,9),(82,14),(82,15);
/*!40000 ALTER TABLE `t_blog_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_comment`
--

DROP TABLE IF EXISTS `t_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_comment` (
  `id` bigint(20) NOT NULL,
  `avatar` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `create_time` datetime(6) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `blog_id` bigint(20) DEFAULT NULL,
  `parent_comment_id` bigint(20) DEFAULT NULL,
  `admin_comment` bit(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FKm8ynu03ijrweyc18ej57ysjwn` (`blog_id`) USING BTREE,
  KEY `FKqr50dgowrlqe2wj5ngj4akwr3` (`parent_comment_id`) USING BTREE,
  CONSTRAINT `FKm8ynu03ijrweyc18ej57ysjwn` FOREIGN KEY (`blog_id`) REFERENCES `t_blog` (`id`),
  CONSTRAINT `FKqr50dgowrlqe2wj5ngj4akwr3` FOREIGN KEY (`parent_comment_id`) REFERENCES `t_comment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_comment`
--

LOCK TABLES `t_comment` WRITE;
/*!40000 ALTER TABLE `t_comment` DISABLE KEYS */;
INSERT INTO `t_comment` VALUES (36,'/images/avatar.jpg','cds','2021-11-30 09:45:38.414000','1840633803@qq.com','cas',18,NULL,_binary '\0'),(37,'/images/avatar.jpg','xzvds','2021-11-30 09:57:21.385000','1840633803@qq.com','cdsc',18,NULL,_binary '\0'),(40,'/images/avatar.jpg','fcewfew','2021-11-30 10:01:52.863000','1840633803@qq.com','fewa',18,NULL,_binary '\0'),(41,'/images/avatar.jpg','fews ','2021-11-30 10:01:59.418000','1840633803@qq.com','fewa',18,NULL,_binary '\0'),(44,'/images/avatar.jpg','feds','2021-11-30 10:02:54.033000','feds','fewsa',18,NULL,_binary '\0'),(45,'/images/avatar.jpg','fewafewsafefw','2021-11-30 10:03:10.025000','fedsfewfaef','fewsafeefewaf',18,NULL,_binary '\0'),(49,'/images/avatar.jpg','你真牛！','2021-11-30 11:02:26.086000','xiaobai@163.com','小白',18,NULL,_binary '\0'),(50,'/images/avatar.jpg','写的不咋样！','2021-11-30 11:10:26.760000','xiaohaung@163.com','小黄',18,NULL,_binary '\0'),(51,'/images/avatar.jpg','写的挺好啊！','2021-11-30 11:30:51.376000','xiaohei@163.com','小黑',18,NULL,_binary '\0'),(52,'https://s3.bmp.ovh/imgs/2021/12/8876c17413a45b85.jpg','抱歉，别黑我了！','2021-11-30 11:31:58.381000','1840633803@qq.com','范占国',18,NULL,_binary ''),(53,'https://s3.bmp.ovh/imgs/2021/12/8876c17413a45b85.jpg','谢谢！','2021-11-30 11:32:08.087000','1840633803@qq.com','范占国',18,NULL,_binary ''),(66,'https://s3.bmp.ovh/imgs/2021/12/8876c17413a45b85.jpg','哇！(#^.^#)','2021-12-01 14:37:48.017000','1840633803@qq.com','范占国',65,NULL,_binary ''),(67,'/images/cutegirl.png','好漂亮！','2021-12-01 14:38:38.433000','xiaohong@qq.com','小红',65,NULL,_binary '\0'),(72,'/images/cutegirl.png','好故事！','2021-12-01 15:01:20.678000','xioahuang@qq.com','小黄',69,NULL,_binary '\0'),(74,'/images/avatar5.jpg','vds','2022-04-15 17:51:44.269000','1840633803@qq.com','fzg',69,NULL,_binary '\0'),(77,'https://s3.bmp.ovh/imgs/2021/12/8876c17413a45b85.jpg','ce','2022-08-03 10:44:03.144000','1840633803@qq.com','范占国',69,NULL,_binary ''),(78,'https://s3.bmp.ovh/imgs/2021/12/8876c17413a45b85.jpg','fewvf Cwsof m侧玩弄','2022-08-03 10:44:17.007000','1840633803@qq.com','范占国',69,74,_binary ''),(79,'/images/avatar7.jpg','fenvi;;jeio;','2022-08-04 14:33:43.930000','fr','rvffr',18,52,_binary '\0'),(80,'/images/avatar4.jpg','fr','2022-08-04 14:33:53.914000','fr','rvffr',18,NULL,_binary '\0'),(81,'/images/avatar7.jpg','发我们来看','2022-08-04 14:34:03.066000','fr','rvffrfr',18,80,_binary '\0');
/*!40000 ALTER TABLE `t_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_tag`
--

DROP TABLE IF EXISTS `t_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_tag` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_tag`
--

LOCK TABLES `t_tag` WRITE;
/*!40000 ALTER TABLE `t_tag` DISABLE KEYS */;
INSERT INTO `t_tag` VALUES (9,'学习'),(10,'代码'),(11,'中二'),(12,'二次元'),(13,'黑'),(14,'漂亮'),(15,'文章'),(70,'激励'),(71,'神话'),(75,'vds');
/*!40000 ALTER TABLE `t_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_type`
--

DROP TABLE IF EXISTS `t_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_type` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_type`
--

LOCK TABLES `t_type` WRITE;
/*!40000 ALTER TABLE `t_type` DISABLE KEYS */;
INSERT INTO `t_type` VALUES (1,'生活'),(2,'数据结构'),(3,'计算机'),(4,'js'),(5,'ts'),(6,'mybatis'),(7,'python'),(8,'springboot'),(19,'markdown'),(68,'故事'),(76,'csa ');
/*!40000 ALTER TABLE `t_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user`
--

DROP TABLE IF EXISTS `t_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_user` (
  `id` bigint(20) NOT NULL,
  `avatar` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `create_time` datetime(6) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user`
--

LOCK TABLES `t_user` WRITE;
/*!40000 ALTER TABLE `t_user` DISABLE KEYS */;
INSERT INTO `t_user` VALUES (1,'https://s3.bmp.ovh/imgs/2021/12/8876c17413a45b85.jpg','2021-11-26 11:14:54.000000','1840633803@qq.com','范占国','202cb962ac59075b964b07152d234b70',1,'2021-11-26 11:14:52.000000','fzg');
/*!40000 ALTER TABLE `t_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'blog'
--

--
-- Dumping routines for database 'blog'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-30 18:14:44
